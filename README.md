# ACS_Spring_23

A repo containing a Jupyter Notebook of Rise slides delivered at the ACS Spring 2023 meeting in Indianapolis, USA.

PAPER ID: 3823529 
PAPER TITLE: Adaptive embedding tackles chemical processes in molecular condensed phases

DIVISION: Division of Computers in Chemistry
SESSION: QM/QM and Embedding Models
SESSION TIME: 2:00 PM - 6:00 PM

DAY & TIME OF PRESENTATION: Sunday, March 26, 2023 from 3:00 PM - 3:30 PM

## Dependencies
 - Use Python 3.10
 - pip install numpy
 - pip install dftpy
 - pip install qepy
 - pip install jupyterlab
 - pip install fortecubeview

You may need to install nglview to render the images. 

## Contact
Michele Pavanello 
 - Email: m.pavanello@rutgers.edu 
 - Twitter: @MikPavanello
